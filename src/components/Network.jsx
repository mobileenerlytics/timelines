import React, { Component } from 'react';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import Helpers from '../utils/helpers';
import Graph from './Graph';

const styles = theme => ({
  card: {
    width: '100%',
    marginTop: 20,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 200,
    verticalAlign: 'baseline',
  },

});

class Network extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      conversation: '',
    };
  }

  componentDidUpdate = (prevProps) => {
    const { thread } = this.props;
    if (prevProps.thread !== thread) {
      if (thread.networks) {
        const { length } = Object.keys(thread.networks);
        if (length > 0) {
          this.setState({ conversation: Object.keys(thread.networks)[length - 1] });
        }
      } else {
        this.setState({ conversation: '' });
      }
    }
  }

  handleExpandClick = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
  }

  handleNetworkConversationChange = (event) => {
    this.setState({ conversation: event.target.value });
  }

  yAxisLabelFormatter = (y) => {
    if (y > 1024 && y < 1024 * 1024) {
      return `${parseFloat(y / 1024).toFixed(1)} KB`;
    }
    if (y > 1024 * 1024 && y < 1024 * 1024 * 1024) {
      return `${parseFloat(y / (1024 * 1024)).toFixed(1)} MB`;
    }
    if (y > 1024 * 1024 * 1024) {
      return `${parseFloat(y / (1024 * 1024 * 1024)).toFixed(1)} GB`;
    }
    return y;
  }

  selectIfNetworkConversation = () => {
    const { classes, thread } = this.props;
    const { conversation } = this.state;
    if (thread.networks) {
      return (
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="networkConversation">Network conversation</InputLabel>
          <Select
            value={conversation}
            onChange={this.handleNetworkConversationChange}
            inputProps={{
              name: 'networkConversation',
            }}
          >
            {Object.keys(thread.networks).sort((a, b) => (thread.networks[b].totalBytes - thread.networks[a].totalBytes)).map((key) => {
              const network = thread.networks[key];
              const ipPort = `${network.remoteIP}:${network.remotePort}`;
              const tcpUdp = `${(network.isUDP) ? 'UDP' : 'TCP'}`;
              const sizeInBytes = parseInt(network.totalBytes, 10);
              let sizeText = '0B';
              if (sizeInBytes > 0 && sizeInBytes <= 1024) {
                sizeText = `${(sizeInBytes).toFixed(2)}B`;
              } else if (sizeInBytes > 1024 && sizeInBytes <= (1024 * 1024)) {
                sizeText = `${(sizeInBytes / (1024)).toFixed(2)}KB`;
              } else if (sizeInBytes > (1024 * 1024)) {
                sizeText = `${(sizeInBytes / (1024 * 1024)).toFixed(2)}MB`;
              }
              if (key === conversation) {
                return (<MenuItem key={key} value={key}><em>{`${ipPort} /${tcpUdp} (${sizeText})`}</em></MenuItem>);
              }
              return (<MenuItem key={key} value={key}>{`${ipPort} /${tcpUdp} (${sizeText})`}</MenuItem>);
            })
            }
          </Select>
        </FormControl>
      );
    }
    return (null);
  }

  headerMenu = () => {
    const { classes } = this.props;
    const { expanded } = this.state;
    return (
      <span>
        {this.selectIfNetworkConversation()}
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </span>
    );
  }

  renderDygraph = () => {
    const {
      thread,
      duration,
      videoCurrentTime,
      videoTimeChange,
      xRange,
      yRange,
      updateRange,
      resetRange,
    } = this.props;
    const { conversation } = this.state;
    if (thread.networks && conversation !== '' && thread.networks[conversation]) {
      const {
        data: pdata,
        title: ptitle,
        labels: plabels,
        colors: pcolors,
      } = Helpers.threadToGraphData(thread, conversation, 'power', duration);
      const {
        data: tdata,
        title: ttitle,
        labels: tlabels,
        colors: tcolors,
      } = Helpers.threadToGraphData(thread, conversation, 'traffic', duration);
      return (
        <div>
          <Graph
            data={pdata}
            title={ptitle}
            labels={plabels}
            colors={pcolors}
            xlabel="Time (s)"
            ylabel="Current (mA)"
            xRange={xRange}
            yRange={yRange}
            videoCurrentTime={videoCurrentTime}
            videoTimeChange={videoTimeChange}
            updateRange={updateRange}
            resetRange={resetRange}
          />
          <Graph
            data={tdata}
            title={ttitle}
            labels={tlabels}
            colors={tcolors}
            xlabel="Time (s)"
            ylabel="Transferred Bytes"
            xRange={xRange}
            videoCurrentTime={videoCurrentTime}
            videoTimeChange={videoTimeChange}
            updateRange={updateRange}
            plotter
            yAxisLabelFormatter={this.yAxisLabelFormatter}
          />
        </div>
      );
    }

    // if (networks && thread.selected) {
    //   return (
    //     <Graph
    //       networks={networks}
    //       duration={duration}
    //       videoCurrentTime={videoCurrentTime}
    //       max={max}
    //       videoTimeChange={videoTimeChange}
    //       xRange={xRange}
    //       updateRange={updateRange}
    //     />
    //   );
    // }
    return (
      <div>
        Selected process did not consume Network energy
      </div>
    );
  }

  render() {
    const {
      classes,
      component,
      updateComponentsInDrawer,
    } = this.props;
    const { expanded } = this.state;

    if (component.inDrawer) {
      return (null);
    }

    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Recipe" style={{ backgroundColor: component.color }}>
              <img src={component.icon} alt="icon" style={{ height: '20px' }} />
            </Avatar>
          )}
          action={this.headerMenu()}
          title={(
            <div>
              <span>{component.name}</span>
              <IconButton onClick={(e) => { e.preventDefault(); updateComponentsInDrawer(component.id, true); }}>
                <ChevronRightIcon />
              </IconButton>
            </div>
          )}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            {this.renderDygraph()}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
export default withStyles(styles)(Network);
