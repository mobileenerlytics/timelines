import React, { Component } from 'react';
import Dygraph from 'dygraphs';
import 'dygraphs/dist/dygraph.min.css';

import Helpers from '../utils/helpers';

class Graph extends Component {
  g = null;

  componentDidMount() {
    const {
      title,
      data,
      labels,
      xlabel,
      ylabel,
      xRange,
      yRange,
      colors,
      // videoCurrentTime,
      videoTimeChange,
      updateRange,
      resetRange,
      plotter,
      stacked,
      yAxisLabelFormatter,
    } = this.props;
    const options = {
      stackedGraph: stacked,
      stepPlot: true,
      fillGraph: true,
      labelsShowZeroValues: false,
      labelsSeparateLines: true,
      legend: 'follow',
      title,
      labels,
      axes: {
        x: {
          valueFormatter: Helpers.valueFormatter,
          axisLabelFormatter: Helpers.valueParser,
        },
      },
      xlabel,
      ylabel,
      dateWindow: xRange,
      colors,
      height: 320,
      interactionModel: {
        ...Dygraph.defaultInteractionModel,
        click: (event, g) => {
          if (event.altKey) {
            const graphPos = Dygraph.findPos(g.graphDiv);
            const canvasx = Dygraph.pageX(event) - graphPos.x;
            const x = g.toDataXCoord(canvasx);
            videoTimeChange(x);
          }
        },
      },
      drawCallback: (me, initial) => {
        if (!initial) {
          updateRange(me.xAxisRange(), me.yAxisRange(), yRange);
        }
      },
    };
    if (resetRange) {
      options.interactionModel.dblclick = () => {
        resetRange();
      };
    }
    if (yRange) {
      options.valueRange = yRange;
    }
    if (plotter) {
      options.plotter = this.barChartPlotter;
    }
    if (yAxisLabelFormatter) {
      options.axes.y = {
        axisLabelFormatter: yAxisLabelFormatter,
      };
    }
    this.g = new Dygraph(this.chart, data, options);
  }

  componentDidUpdate(prevProps) {
    const {
      data,
      videoCurrentTime,
      xRange,
      yRange,
      labels,
      title,
      colors,
      stacked,
    } = this.props;
    const updateOptions = {};
    if (prevProps.stacked !== stacked) {
      updateOptions.stackedGraph = stacked;
    }
    if (prevProps.xRange[0] !== xRange[0] || prevProps.xRange[1] !== xRange[1]) {
      updateOptions.dateWindow = xRange;
    }

    if (yRange && (prevProps.yRange[0] !== yRange[0] || prevProps.yRange[1] !== yRange[1])) {
      updateOptions.valueRange = yRange;
    }

    if (prevProps.videoCurrentTime !== videoCurrentTime && videoCurrentTime) {
      updateOptions.underlayCallback = (canvas, area, g) => {
        const newCanvas = canvas;
        const xxRange = g.xAxisRange();
        const thickness = (xxRange[1] - xxRange[0]) / 2000;
        const left = g.toDomCoords(videoCurrentTime - thickness, -20)[0];
        const right = g.toDomCoords(videoCurrentTime + thickness, +20)[0];
        newCanvas.fillStyle = 'rgba(0, 0, 0, 1.0)';
        newCanvas.fillRect(left, area.y, right - left, area.h);
      };
    }
    if (prevProps.data !== data) {
      updateOptions.file = data;
      updateOptions.labels = labels;
      updateOptions.title = title;
      updateOptions.colors = colors;
      updateOptions.dateWindow = xRange;
      if (yRange) {
        updateOptions.valueRange = yRange;
      }
    }
    this.g.updateOptions(updateOptions);
  }

  barChartPlotter = (e) => {
    const ctx = e.drawingContext;
    const { points } = e;
    const yBottom = e.dygraph.toDomYCoord(0);

    ctx.fillStyle = e.color;

    // Find the minimum separation between x-values.
    // This determines the bar width.
    let minSep = Infinity;
    for (let i = 1; i < points.length; i += 1) {
      const sep = points[i].canvasx - points[i - 1].canvasx;
      if (sep < minSep) minSep = sep;
    }
    const widthBySep = Math.floor(2.0 / 3 * minSep);
    const barWidth = Math.min(1, widthBySep);

    // Do the actual plotting.
    for (let i = 0; i < points.length; i += 1) {
      const p = points[i];
      const centerX = p.canvasx;

      ctx.fillRect(centerX - barWidth / 2, p.canvasy,
        barWidth, yBottom - p.canvasy);

      ctx.strokeRect(centerX - barWidth / 2, p.canvasy,
        barWidth, yBottom - p.canvasy);
    }
  }

  render() {
    return <div ref={(c) => { this.chart = c; }} />;
  }
}
export default Graph;
