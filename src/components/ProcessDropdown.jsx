import React from 'react';
import Select from 'react-select';

import Helpers from '../utils/helpers';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  root: {
    flexGrow: 1,
    maxWidth: 500,
  },
  input: {
    display: 'flex',
    padding: 0,
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
    color: '#ffffff',
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
    color: '#ffffff',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
});

function convertToOrderedList(list) {
  const orderedParentProcessesId = Object.keys(list).sort((a, b) => (list[b].totalEnergy - list[a].totalEnergy));
  const orderedList = [];
  for (let i = 0; i < orderedParentProcessesId.length; i += 1) {
    const id = parseInt(orderedParentProcessesId[i], 10);
    if (list[id].totalEnergy > 0) {
      orderedList.push({
        value: id,
        label: `${list[id].name} [${list[id].totalEnergy.toFixed(2)} uAh]`,
      });
    }
  }
  return orderedList;
}

function NoOptionsMessage(props) {
  const { selectProps, innerProps, children } = props;
  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.noOptionsMessage}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  const {
    selectProps, innerRef, innerProps, children,
  } = props;
  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: selectProps.classes.input,
          inputRef: innerRef,
          children,
          ...innerProps,
        },
      }}
      {...selectProps.textFieldProps}
    />
  );
}


function Option(props) {
  const {
    innerRef, isFocused, isSelected, innerProps, children,
  } = props;
  return (
    <MenuItem
      buttonRef={innerRef}
      selected={isFocused}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
      {...innerProps}
    >
      {children}
    </MenuItem>
  );
}


function Placeholder(props) {
  const { selectProps, innerProps, children } = props;
  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.placeholder}
      {...innerProps}
    >
      {children}
    </Typography>
  );
}

function SingleValue(props) {
  const { selectProps, innerProps, children } = props;
  return (
    <Typography className={selectProps.classes.singleValue} {...innerProps}>
      {children}
    </Typography>
  );
}

function ValueContainer(props) {
  const { children, selectProps } = props;
  return (<div className={selectProps.classes.valueContainer}>{children}</div>);
}

function Menu(props) {
  const { children, selectProps, innerProps } = props;
  return (
    <Paper square className={selectProps.classes.paper} {...innerProps}>
      {children}
    </Paper>
  );
}

const components = {
  Control,
  Menu,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer,
};

class ProcessDropdown extends React.PureComponent {
  render() {
    const {
      classes, value, onChange, processes,
    } = this.props;
    const list = convertToOrderedList(processes);
    const defaultValueId = Helpers.objSearch(list, value, 'value');
    let defaultValue = null;
    if (defaultValueId >= 0) {
      defaultValue = list[defaultValueId];
    }
    return (
      <div className={classes.root}>
        <Select
          classes={classes}
          value={defaultValue}
          onChange={onChange}
          options={list}
          components={components}
          placeholder="Search a process"
        />
      </div>
    );
  }
}

export default withStyles(styles)(ProcessDropdown);
