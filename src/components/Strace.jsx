import React, { Component } from 'react';
import memoize from 'memoize-one';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControl from '@material-ui/core/FormControl';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import StraceInfiniteList from './StraceList';

const styles = theme => ({
  card: {
    width: '100%',
    marginTop: 20,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    verticalAlign: 'baseline',
  },
  valueLabel: {
    marginRight: 20,
  },
});

class Strace extends Component {
  filter = memoize(
    (threads, search) => {
      const filteredThread = {};
      Object.keys(threads).forEach((tkey) => {
        const filteredThreadStrace = {};
        if (threads[tkey].selected && threads[tkey].strace) {
          Object.keys(threads[tkey].strace).forEach((time) => {
            filteredThreadStrace[time] = threads[tkey].strace[time].filter(item => item.toLowerCase().includes(search.toLowerCase()));
          });
          filteredThread[tkey] = { ...threads[tkey] };
          filteredThread[tkey].strace = filteredThreadStrace;
        }
      });
      return filteredThread;
    },
  );

  straceList = [];

  timeList = [];

  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      search: '',
      count: 0,
    };
  }

  handleExpandClick = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
  }

  headerMenu = (count) => {
    const { classes } = this.props;
    const { expanded, search } = this.state;
    return (
      <span>
        <Typography className={classes.valueLabel} inline variant="subheading" align="center">
          Total:
          {count}
        </Typography>
        <FormControl className={classes.formControl}>
          <TextField
            id="standard-search"
            label="Search field"
            type="search"
            className={classes.textField}
            margin="none"
            value={search}
            onChange={this.handleSearchChange}
          />
        </FormControl>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </span>
    );
  }

  handleSearchChange = (event) => {
    this.setState({ search: event.target.value });
  }

  mapToList = (threads) => {
    const finalList = [];
    const createTimeList = [];
    Object.keys(threads).forEach((tkey) => {
      Object.keys(threads[tkey].strace).forEach((time) => {
        threads[tkey].strace[time].forEach((strac) => {
          const log = strac.split(':');
          const intTime = parseInt(time, 10);
          createTimeList.push(intTime);
          finalList.push({
            timestamp: intTime,
            call: log[0],
            count: log[1],
            error: log[2],
            thread: tkey,
            color: threads[tkey].color,
          });
        });
      });
    });
    this.timeList = createTimeList.sort((a, b) => a - b);
    this.straceList = finalList.sort((a, b) => (a.timestamp - b.timestamp));
  }

  componentDidMount = () => {
    const { videoCurrentTime, threads } = this.props;
    const { search, expanded } = this.state;
    const filteredObj = this.filter(threads, search);
    this.mapToList(filteredObj, videoCurrentTime);
    if (this.straceList.length && !expanded) {
      this.setState({ expanded: true });
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { threads } = this.props;
    const { search } = this.state;
    if (prevProps.threads !== threads || prevState.search !== search) {
      const filteredObj = this.filter(threads, search);
      this.mapToList(filteredObj);
      this.setState({ count: this.calculateCount(this.straceList) });
    }
    // if (this.logRef && this.logRefParent && (videoCurrentTime !== prevProps.videoCurrentTime)) {
    //   this.logRefParent.scrollTo(0, this.logRef.offsetTop - this.logRefParent.offsetTop);
    // }
  }

  calculateCount = (straceList) => {
    let count = 0;
    straceList.forEach((item) => {
      count += parseInt(item.count, 10);
    });
    return count;
  }

  getSelectAndActiveStrace = () => {
    const { videoCurrentTime } = this.props;
    const { timeList } = this;
    let index = 0;
    let time = 0;
    while (timeList[index] <= videoCurrentTime) {
      index += 1;
    }
    time = timeList[index];
    while (time && time === timeList[index + 1]) {
      index += 1;
    }
    return {
      select: time || 0,
      active: index,
    };
  }

  render() {
    const { classes, component, updateComponentsInDrawer } = this.props;
    const { expanded, count } = this.state;
    const selectAndActive = this.getSelectAndActiveStrace();

    if (component.inDrawer) {
      return (null);
    }

    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Strace" style={{ backgroundColor: component.color }}>
              <img src={component.icon} alt="icon" style={{ height: '20px' }} />
            </Avatar>
          )}
          action={this.headerMenu(count)}
          title={(
            <div>
              <span>{component.name}</span>
              <IconButton onClick={(e) => { e.preventDefault(); updateComponentsInDrawer(component.id, true); }}>
                <ChevronRightIcon />
              </IconButton>
            </div>
          )}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <StraceInfiniteList
              list={this.straceList}
              select={selectAndActive.select}
              active={selectAndActive.active}
            />
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
export default withStyles(styles)(Strace);
