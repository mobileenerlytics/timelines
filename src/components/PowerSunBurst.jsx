import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { phoneIndex } from '../config';

import SunBurst from './SunBurst';
import Helpers from '../utils/helpers';

const styles = () => ({
  root: {
    width: '100%',
    display: 'flex',
    margin: '0 auto',
  },
});

class PowerSunBurst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prevProps: {},
      data: {},
    };
  }

  static getComponentColor = (key) => {
    switch (key) {
      case 'CPU':
        return '#5d6bb9';
      case 'GPU':
        return '#42a5f5';
      case 'SCREEN':
        return '#29b6f6';
      case 'Network':
        return '#26a69a';
      case 'GPS':
        return '#ef5350';
      default:
        return '#999999';
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { threads, processesSelectedValue, processes, totalEnergy } = props;
    const prevProps = state.prevProps || {};
    let increment = 0;
    if (prevProps !== props) {
      const data = {};
      if (processesSelectedValue === phoneIndex) {
        const newProcesses = { ...processes };
        const mainProcesses = newProcesses[phoneIndex];
        const restProcesses = newProcesses;
        delete restProcesses[phoneIndex];
        data.name = mainProcesses.name;
        data.title = mainProcesses.name;
        data.hex = mainProcesses.threads[phoneIndex].color;
        data.power = totalEnergy;
        data.children = [];
        Object.keys(restProcesses).forEach((id) => {
          const process = restProcesses[id];
          if (process.totalEnergy > 0) {
            const child = {
              name: process.name + increment,
              title: process.name,
              hex: process.threads[id].color,
              power: process.totalEnergy,
              children: [],
            };
            increment += 1;
            Object.keys(process.threads).forEach((key) => {
              const thread = process.threads[key];
              Object.keys(thread.components).forEach((ckey) => {
                const component = thread.components[ckey];
                const index = Helpers.objSearch(child.children, ckey, 'title');
                if (index >= 0) {
                  child.children[index].value += component;
                  child.children[index].power += component;
                } else {
                  const componentChild = {
                    name: ckey + increment,
                    title: ckey,
                    hex: PowerSunBurst.getComponentColor(ckey),
                    value: component,
                    power: component,
                  };
                  increment += 1;
                  child.children.push(componentChild);
                }
              });
            });
            data.children.push(child);
          }
        });
        return { data };
      }
      const newThreads = { ...threads };
      const mainThread = newThreads[processesSelectedValue];
      const restThreads = newThreads;
      // delete restThreads[processesSelectedValue];
      data.name = mainThread.name;
      data.title = mainThread.name;
      data.hex = mainThread.color;
      data.power = totalEnergy;
      data.children = [];
      Object.keys(restThreads).forEach((id) => {
        const thread = restThreads[id];
        const child = {
          name: thread.name + increment,
          title: thread.name,
          hex: thread.color,
          power: thread.energy,
          children: [],
        };
        increment += 1;
        Object.keys(thread.components).forEach((key) => {
          const component = thread.components[key];
          const componentChild = {
            name: key + increment,
            title: key,
            hex: PowerSunBurst.getComponentColor(key),
            value: component,
            power: component,
          };
          increment += 1;
          child.children.push(componentChild);
        });
        data.children.push(child);
      });
      return { data };
    }
    return null;
  }

  render() {
    const { classes, totalEnergy } = this.props;
    const { data } = this.state;
    const newData = { children: [data] };
    return (
      <div className={classes.root}>
        <SunBurst data={newData} totalEnergy={totalEnergy} />
      </div>
    );
  }
}
export default withStyles(styles)(PowerSunBurst);
