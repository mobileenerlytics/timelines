import React, { Component } from 'react';
import memoize from 'memoize-one';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const styles = theme => ({
  card: {
    width: '100%',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    verticalAlign: 'baseline',
  },
  valueLabel: {
    marginRight: 20,
  },
  logcatData: {
    minHeight: 180,
    width: '100%',
  },
  selectedLog: {
    backgroundColor: '#dddddd',
  },
  log: {
    fontSize: '1.2em',
  },
});

const logTypes = [{
  key: 'VFEWID',
  value: 'Verbose',
}, {
  key: 'FEWID',
  value: 'Debug',
}, {
  key: 'FEWI',
  value: 'Info',
}, {
  key: 'FEW',
  value: 'Warning',
}, {
  key: 'FE',
  value: 'Error',
}, {
  key: 'F',
  value: 'Fatal',
}];

class LogCat extends Component {
  filter = memoize(
    (object, search, logType) => {
      const filteredObject = {};
      Object.keys(object).forEach((key) => {
        filteredObject[key] = object[key]
          .filter(item => item.toLowerCase().includes(search.toLowerCase()))
          .filter(item => logType.includes(item.substring((item.indexOf('/') - 1), item.indexOf('/'))));
      });
      return filteredObject;
    },
  );

  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      search: '',
      logType: 'VFEWID',
    };
  }

  mapToSelect = (object, videoCurrentTime) => {
    const { classes, videoTimeChange } = this.props;
    let onceMarked = false;
    const finalHTMLList = [];
    let indexCounter = 1;
    Object.keys(object).forEach((key) => {
      const selectTime = parseInt(key, 10);
      if (videoCurrentTime <= selectTime && !onceMarked) {
        object[key].forEach((item) => {
          if (!onceMarked) {
            finalHTMLList
              .push(
                <option
                  onDoubleClick={() => { videoTimeChange(parseInt(key, 10)); }}
                  ref={(c) => { this.logRef = c; }}
                  className={classnames(classes.selectedLog, classes.log)}
                  key={indexCounter += 1}
                >
                  {`${key} : ${item}`}
                </option>,
              );
          } else {
            finalHTMLList
              .push(
                <option
                  onDoubleClick={() => { videoTimeChange(parseInt(key, 10)); }}
                  className={classnames(classes.selectedLog, classes.log)}
                  key={indexCounter += 1}
                >
                  {`${key} : ${item}`}
                </option>,
              );
          }
          onceMarked = true;
        });
      } else {
        object[key].forEach((item) => {
          finalHTMLList.push(
            <option
              onDoubleClick={() => { videoTimeChange(parseInt(key, 10)); }}
              className={classes.log}
              key={indexCounter += 1}
            >
              {`${key} : ${item}`}
            </option>,
          );
        });
      }
    });
    return finalHTMLList;
  }

  handleExpandClick = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
  }

  headerMenu = (count) => {
    const { classes } = this.props;
    const { expanded, search, logType } = this.state;
    return (
      <span>
        <Typography className={classes.valueLabel} inline variant="subheading" align="center">{`Total: ${count}`}</Typography>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="logType">Log Type</InputLabel>
          <Select
            value={logType}
            onChange={this.handleLogTypeChange}
            inputProps={{
              name: 'logType',
            }}
          >
            {logTypes.map(item => ((item.key === logType)
              ? <MenuItem key={item.key} value={item.key}><em>{item.value}</em></MenuItem>
              : <MenuItem key={item.key} value={item.key}>{item.value}</MenuItem>
            ))
            }
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <TextField
            id="standard-search"
            label="Search field"
            type="search"
            className={classes.textField}
            margin="none"
            value={search}
            onChange={this.handleSearchChange}
          />
        </FormControl>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </span>
    );
  }

  handleSearchChange = (event) => {
    this.setState({ search: event.target.value });
  }

  handleLogTypeChange = (event) => {
    this.setState({ logType: event.target.value });
  }

  componentDidUpdate = (prevProps) => {
    const { videoCurrentTime } = this.props;
    if (this.logRef && this.logRefParent && (videoCurrentTime !== prevProps.videoCurrentTime)) {
      this.logRefParent.scrollTo(0, this.logRef.offsetTop - this.logRefParent.offsetTop);
    }
  }

  renderLogs() {
    const { videoCurrentTime, classes, logcat } = this.props;
    const { search, logType } = this.state;
    const filteredList = this.filter(logcat, search, logType);
    if (Object.keys(filteredList).length > 0) {
      const selectedList = this.mapToSelect(filteredList, videoCurrentTime);
      return (
        <select ref={(c) => { this.logRefParent = c; }} multiple className={classes.logcatData}>
          {selectedList}
        </select>
      );
    }
    return (
      <div>
        No logs for selected process
      </div>
    );
  }

  render() {
    const { videoCurrentTime, classes, logcat, component, updateComponentsInDrawer } = this.props;
    const { expanded, search, logType } = this.state;
    const filteredList = this.filter(logcat, search, logType);
    const selectedList = this.mapToSelect(filteredList, videoCurrentTime);

    if (component.inDrawer) {
      return (null);
    }
    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Recipe" style={{ backgroundColor: component.color }}>
              <img src={component.icon} alt="icon" style={{ height: '20px' }} />
            </Avatar>
          )}
          action={this.headerMenu(selectedList.length)}
          title={(
            <div>
              <span>{component.name}</span>
              <IconButton onClick={(e) => { e.preventDefault(); updateComponentsInDrawer(component.id, true); }}>
                <ChevronRightIcon />
              </IconButton>
            </div>
          )}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            {this.renderLogs()}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
export default withStyles(styles)(LogCat);
