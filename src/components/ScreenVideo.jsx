import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { endpoints } from '../config';

const styles = () => ({
  root: {
    width: '100%',
    display: 'flex',
    margin: '0 auto',
  },
});

class ScreenVideo extends Component {
  constructor(props) {
    super(props);
    this.video = null;
  }

  onTimeUpdate = () => {
    const { videoTimeChange } = this.props;
    videoTimeChange(this.video.currentTime * 1000);
  }

  componentDidUpdate = () => {
    const { videoCurrentTime } = this.props;
    if (this.video.currentTime * 1000 !== videoCurrentTime) {
      this.video.currentTime = videoCurrentTime / 1000;
    }
  }

  render() {
    const { classes, id, projectId } = this.props;
    return (
      <video muted className={classes.root} controls onTimeUpdate={this.onTimeUpdate} ref={(c) => { this.video = c; }}>
        <source src={`${endpoints.processes.url}${id}/screen.mp4?projectId=${projectId}`} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    );
  }
}
export default withStyles(styles)(ScreenVideo);
