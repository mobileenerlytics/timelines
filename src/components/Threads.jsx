import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from '@material-ui/core/Chip';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import DoneIcon from '@material-ui/icons/Done';
import ClearIcon from '@material-ui/icons/Clear';

const styles = theme => ({
  card: {
    width: '100%',
    marginTop: 20,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  chip: {
    margin: theme.spacing.unit,
  },
  childChip: {
    color: '#ffffff',
  },
});

class Threads extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
    };
  }

  handleExpandClick = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
  }

  headerMenu = () => {
    const { classes, threads, childProcessSelectAll } = this.props;
    const { expanded } = this.state;
    let childprocessselect = true;
    Object.keys(threads).forEach((key) => {
      childprocessselect = childprocessselect && threads[key].selected;
    });
    return (
      <span>
        <Chip
          icon={(childprocessselect) ? <ClearIcon /> : <DoneIcon />}
          label={(childprocessselect) ? 'Select none' : 'Select all'}
          clickable
          color="default"
          onClick={() => { childProcessSelectAll(!childprocessselect); }}
        />
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </span>
    );
  }

  render() {
    const { classes, threads, childProcessSelectChange, component, updateComponentsInDrawer } = this.props;
    const { expanded } = this.state;

    if (component.inDrawer) {
      return (null);
    }
    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Recipe" style={{ backgroundColor: component.color }}>
              <img src={component.icon} alt="icon" style={{ height: '20px' }} />
            </Avatar>
          )}
          action={this.headerMenu()}
          title={(
            <div>
              <span>{component.name}</span>
              <IconButton onClick={(e) => { e.preventDefault(); updateComponentsInDrawer(component.id, true); }}>
                <ChevronRightIcon />
              </IconButton>
            </div>
          )}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            { Object.keys(threads).sort((a, b) => (threads[b].energy - threads[a].energy)).map(key => (
              <Chip
                icon={(threads[key].selected) ? <DoneIcon style={{ color: threads[key].color }} /> : <ClearIcon style={{ color: threads[key].color }} />}
                label={`${threads[key].name} ${threads[key].energy.toFixed(2)} uAh`}
                key={threads[key].name}
                clickable
                color="default"
                style={{ border: `1px solid ${threads[key].color}`, color: threads[key].color }}
                className={classnames(classes.chip, classes.childChip)}
                onClick={() => childProcessSelectChange(key)}
                variant="outlined"
              />
            ))}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
export default withStyles(styles)(Threads);
