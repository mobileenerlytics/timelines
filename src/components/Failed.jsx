import React from 'react';

import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  root: {
    textAlign: 'center',
    width: '100%',
    height: '100vh',
    display: 'table',
  },
  text: {
    display: 'table-cell',
    verticalAlign: 'middle',
  },
});

class Failed extends React.PureComponent {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.text}>
          We could not find any data for this ID. Please contact us at support@mobileenerlytics.com if you have any questions.
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Failed);
