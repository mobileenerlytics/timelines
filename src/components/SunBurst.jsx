import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { Sunburst, LabelSeries } from 'react-vis';
import classnames from 'classnames';

const styles = () => ({
  root: {
    width: '100%',
    margin: '0 auto',
  },
  breadcrumb: {
    margin: '20px',
  },
});

const primaryLabel = {
  fontSize: '16px',
  textAnchor: 'middle',
};
const secondaryLabel = {
  fontSize: '12px',
  textAnchor: 'middle',
};

/**
 * Recursively work backwards from highlighted node to find path of valud nodes
 * @param {Object} node - the current node being considered
 * @returns {Array} an array of strings describing the key route to the current node
 */

function getKey(node, type) {
  if (!node.parent) {
    return ['root'];
  }

  return [(node.data && node.data[type]) || node[type]].concat(
    getKey(node.parent, type),
  );
}

class PowerSunBurst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pathValue: [],
      pathColor: [],
      data: this.decoratedData(),
      finalValue: false,
      clicked: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.setState({
        data: this.decoratedData(),
      });
    }
  }

  getBreadcrubHTML() {
    const { pathValue, pathColor } = this.state;
    const newList = pathValue.filter((item, index) => (index !== 0));
    if (newList.length < 1) {
      return null;
    }
    return (
      <ul className="breadcrumb">
        {newList.map((item, index) => (
          <li key={item} className={classnames({ only: newList.length === 1 })}>
            <span style={{ backgroundColor: pathColor[index + 1], border: `0 solid ${pathColor[index + 1]}` }}>
              {item}
            </span>
          </li>
        ))}
      </ul>
    );
  }

  /**
 * Recursively modify data depending on whether or not each cell has been selected by the hover/highlight
 * @param {Object} data - the current node being considered
 * @param {Object|Boolean} keyPath - a map of keys that are in the highlight path
 * if this is false then all nodes are marked as selected
 * @returns {Object} Updated tree structure
 */
  updateData = (data, keyPath) => {
    const newData = data;
    if (newData.children) {
      newData.children.map(child => this.updateData(child, keyPath));
    }
    // add a fill to all the uncolored cells
    if (!newData.hex) {
      newData.style = {
        fill: '#666666',
      };
    }
    newData.style = {
      ...newData.style,
      fillOpacity: keyPath && !keyPath[newData.name] ? 0.2 : 1,
    };

    return newData;
  }

  decoratedData = () => {
    const { data } = this.props;
    return this.updateData(data, false);
  }

  render() {
    const {
      clicked,
      data,
      finalValue,
    } = this.state;
    const { classes, totalEnergy } = this.props;
    return (
      <div className={classes.root}>
        <Sunburst
          animation={{ damping: 20, stiffness: 300 }}
          className="basic-sunburst"
          hideRootNode
          padAngle={() => 0.01}
          onValueMouseOver={(node) => {
            if (clicked) {
              return;
            }
            const path = getKey(node, 'name').reverse();
            const color = getKey(node, 'hex').reverse();
            const title = getKey(node, 'title').reverse();
            const pathAsMap = path.reduce((res, row) => {
              res[row] = true;
              return res;
            }, {});
            const { power } = node;
            const fValue = [(power / totalEnergy * 100).toFixed(2), power.toFixed(2), totalEnergy.toFixed(2)];
            this.setState({
              finalValue: fValue,
              pathValue: title,
              pathColor: color,
              data: this.updateData(this.decoratedData(), pathAsMap),
            });
          }}
          onValueMouseOut={() => {
            if (!clicked) {
              this.setState({
                pathValue: [],
                pathColor: [],
                finalValue: false,
                data: this.updateData(this.decoratedData(), false),
              });
            }
          }}
          onValueClick={() => this.setState({ clicked: !clicked })}
          style={{
            stroke: '#ddd',
            strokeOpacity: 0.3,
            strokeWidth: '0.5',
          }}
          colorType="literal"
          getSize={d => d.value}
          getColor={d => d.hex}
          data={data}
          height={300}
          width={350}
        >
          {finalValue && (
            <LabelSeries
              data={[
                {
                  x: 0,
                  y: 25,
                  label: `${finalValue[0]}%`,
                  style: primaryLabel,
                },
                {
                  x: 0,
                  y: -10,
                  label: `${finalValue[1]} uAh`,
                  style: secondaryLabel,
                },
                {
                  x: 0,
                  y: -25,
                  label: `out of ${finalValue[2]} uAh`,
                  style: secondaryLabel,
                },
              ]}
            />
          )}
        </Sunburst>
        <div className={classes.breadcrumb}>{this.getBreadcrubHTML()}</div>
      </div>
    );
  }
}
export default withStyles(styles)(PowerSunBurst);
