import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import logo from '../resources/logo.png';

const styles = () => ({
  root: {
    textAlign: 'center',
    width: '100%',
    height: '100vh',
  },
  helper: {
    display: 'inline-block',
    height: '100%',
    verticalAlign: 'middle',
  },
  img: {
    verticalAlign: 'middle',
  },
});

class Loading extends React.PureComponent {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root} suppressHydrationWarning>
        <span className={classes.helper} />
        <img className={classes.img} src={logo} alt="logo" />
      </div>
    );
  }
}

export default withStyles(styles)(Loading);
