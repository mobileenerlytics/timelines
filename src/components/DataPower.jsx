import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import Helpers from '../utils/helpers';
import Graph from './Graph';

const styles = theme => ({
  card: {
    width: '100%',
    marginTop: 20,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class DataPower extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
    };
  }

  handleExpandClick = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
  }

  headerMenu = () => {
    const { classes } = this.props;
    const { expanded } = this.state;
    return (
      <span>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </span>
    );
  }

  renderDygraph = () => {
    const {
      threads,
      component,
      duration,
      videoCurrentTime,
      videoTimeChange,
      updateRange,
      xRange,
      yRange,
      resetRange,
      stacked,
    } = this.props;
    const {
      data,
      title,
      labels,
      colors,
    } = Helpers.threadsToGraphData(threads, component.datafor, duration);
    let dataAvailable = false;
    Object.keys(threads).forEach((key) => {
      if (threads[key][component.datafor] && threads[key].selected) {
        dataAvailable = true;
      }
    });
    if (dataAvailable) {
      return (
        <Graph
          stacked={stacked}
          data={data}
          title={title}
          labels={labels}
          colors={colors}
          xlabel="Time (s)"
          ylabel="Current (mA)"
          xRange={xRange}
          yRange={yRange}
          videoCurrentTime={videoCurrentTime}
          videoTimeChange={videoTimeChange}
          updateRange={updateRange}
          resetRange={resetRange}
        />
      );
    }
    return (
      <div>
        Selected process did not consume
        {` ${component.name} `}
        energy
      </div>
    );
  }

  render() {
    const {
      classes,
      component,
      updateComponentsInDrawer,
    } = this.props;
    const { expanded } = this.state;

    if (component.inDrawer) {
      return (null);
    }
    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Recipe" style={{ backgroundColor: component.color }}>
              <img src={component.icon} alt="icon" style={{ height: '20px' }} />
            </Avatar>
          )}
          action={this.headerMenu()}
          title={(
            <div>
              <span>{component.name}</span>
              <IconButton onClick={(e) => { e.preventDefault(); updateComponentsInDrawer(component.id, true); }}>
                <ChevronRightIcon />
              </IconButton>
            </div>
          )}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            {this.renderDygraph()}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
export default withStyles(styles)(DataPower);
