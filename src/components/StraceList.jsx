import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';

import { AutoSizer, List } from 'react-virtualized';

const styles = () => ({
  selectedStrace: {
    backgroundColor: '#eeeeee',
  },

  straceTableRow: {
    display: 'inline-flex',
    marginLeft: '10px',
    width: '160px',
    whiteSpace: 'nowrap',
  }
});

class StraceList extends Component {
  rowRender = ({ index, key, style }) => {
    const { classes } = this.props;
    const { list, select } = this.props;
    const data = list[index];
    const { color } = data;
    const selectRow = (select === data.timestamp);
    return (
      <div className={classnames({ [classes.selectedStrace]: selectRow })} key={key} style={style}>
        <div className={classes.straceTableRow} style={{ color }}>
          {data.timestamp}
          ms
        </div>
        <div className={classes.straceTableRow} style={{ color }}>{data.call}</div>
        <div className={classes.straceTableRow} style={{ color }}>{data.count}</div>
        <div className={classes.straceTableRow} style={{ color }}>{data.error}</div>
      </div>
    );
  }

  displayStrace = () => {

  }

  render() {
    const { classes, list, active } = this.props;
    if (list.length > 0) {
      return (
        <div>
          <div id="strace-table-header">
            <div className={classes.straceTableRow} style={{ fontWeight: 'bold' }}>Timestamp</div>
            <div className={classes.straceTableRow} style={{ fontWeight: 'bold' }}>System call</div>
            <div className={classes.straceTableRow} style={{ fontWeight: 'bold' }}>Number of calls</div>
            <div className={classes.straceTableRow} style={{ fontWeight: 'bold' }}>Errors</div>
          </div>
          <AutoSizer
            disableHeight={true}>
            {({ width }) => (
              <List
                width={width}
                height={250}
                rowHeight={20}
                rowCount={list.length}
                rowClassName={classes.straceTableRow}
                headerClassName="strace-table-col"
                rowRenderer={this.rowRender}
                overscanRowCount={20}
                scrollToIndex={active}
              />
            )}
          </AutoSizer>
        </div>
      );
    }
    return (
      <div>
        There are no Strace to display
      </div>
    );
  }
}
export default withStyles(styles)(StraceList);
