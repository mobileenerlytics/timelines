import React, { Component } from 'react';
import memoize from 'memoize-one';
import Chart from 'react-google-charts';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Chip from '@material-ui/core/Chip';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControl from '@material-ui/core/FormControl';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const styles = theme => ({
  card: {
    width: '100%',
    marginTop: 20,
  },
  content: {
    position: 'relative',
  },
  cursor: {
    position: 'absolute',
    top: 0,
    left: '120px',
    borderLeft: '1.5px solid black',
    height: '200px',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    verticalAlign: 'baseline',
  },
  chip: {
    margin: theme.spacing.unit,
  },
});

const format = [
  { type: 'string', id: 'Event' },
  { type: 'string', id: 'name' },
  { type: 'number', id: 'Start' },
  { type: 'number', id: 'End' },
];

class Events extends Component {
  filter = memoize(
    (events, search, selected) => {
      const filteredEvents = events
        .filter(event => event[1].toLowerCase().includes(search.toLowerCase()))
        .filter((event) => {
          let returnValue = true;
          selected.forEach((item) => {
            if (item[0] === event[0]) {
              returnValue = (item[1] === event[1]);
            }
          });
          return returnValue;
        });
      const data = filteredEvents.map(event => event.slice(0, -1));
      const colors = filteredEvents.map(event => event[event.length - 1]);
      return { data, colors };
    },
  );

  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      search: '',
      selected: [],
    };
  }

  handleExpandClick = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
  }

  headerMenu = () => {
    const { classes } = this.props;
    const { expanded, search } = this.state;
    return (
      <span>
        <FormControl className={classes.formControl}>
          <TextField
            id="standard-search"
            label="Search field"
            type="search"
            className={classes.textField}
            margin="none"
            value={search}
            onChange={this.handleSearchChange}
          />
        </FormControl>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </span>
    );
  }

  handleSearchChange = (event) => {
    this.setState({ search: event.target.value });
  }

  componentDidMount = () => {
    const { thread } = this.props;
    if (thread.events) {
      this.setState({ expanded: true });
    }
  }

  handleSelect = (events, index) => {
    const { selected } = this.state;
    const newSelected = [...selected];
    const clicked = events.data[index];
    const color = events.colors[index];
    let haveToSetState = true;
    selected.forEach((item) => {
      if (item[0] === clicked[0] && item[1] === clicked[1]) {
        haveToSetState = false;
      }
    });
    if (haveToSetState) {
      newSelected.push([clicked[0], clicked[1], color]);
      this.setState({ selected: newSelected });
    }
  }

  deleteSelected = (removeSelected) => {
    const { selected } = this.state;
    const newSelected = selected.filter(item => !(removeSelected[0] === item[0] && removeSelected[1] === item[1]));
    this.setState({ selected: newSelected });
  }

  getCursorLocation = (duration, videoCurrentTime) => {
    let element = null;
    if (document && document.getElementsByClassName('eventChart').length > 0) {
      element = document.getElementsByClassName('eventChart')[0];
    }
    let rect = '';
    let pos = '';
    if (element && element.getElementsByTagName('div')[2] && element.getElementsByTagName('div')[2].getElementsByTagName('svg')[0] && element.getElementsByTagName('div')[2].getElementsByTagName('svg')[0].getElementsByTagName('g')[3]) {
      rect = element.getElementsByTagName('div')[2].getElementsByTagName('svg')[0].getElementsByTagName('g')[3];
    } else if (element && element.getElementsByTagName('div')[3] && element.getElementsByTagName('div')[3].getElementsByTagName('svg')[0] && element.getElementsByTagName('div')[3].getElementsByTagName('svg')[0].getElementsByTagName('g')[2]) {
      rect = element.getElementsByTagName('div')[3].getElementsByTagName('svg')[0].getElementsByTagName('g')[2];
    } else {
      return 0;
    }

    pos = rect.getBBox().x;
    const containerPos = element.offsetLeft;
    const { width } = rect.getBBox();
    return pos + containerPos + ((width) * videoCurrentTime) / duration;
  }

  renderGraph() {
    const {
      classes,
      thread,
      videoCurrentTime,
      duration,
    } = this.props;
    const {
      search,
      selected,
    } = this.state;
    const cursorLocation = this.getCursorLocation(duration, videoCurrentTime);
    const events = this.filter(thread.events || [], search, selected);
    if (events.data.length > 0) {
      return (
        <div>
          <Chart
            className="eventChart"
            width="match-parent"
            chartType="Timeline"
            data={[format].concat(events.data)}
            options={{
              timeline: {
                barLabelStyle: { opacity: 0 },
              },
              colors: events.colors,
            }}
            chartEvents={[
              {
                eventName: 'select',
                callback: (googleChart) => {
                  const { chartWrapper } = googleChart;
                  const chart = chartWrapper.getChart();
                  const [selectedItem] = chart.getSelection();
                  this.handleSelect(events, selectedItem.row);
                },
              },
            ]}
            rootProps={{ 'data-testid': '1' }}
          />
          {(cursorLocation) ? <div className={classes.cursor} style={{ left: cursorLocation }} /> : null}
          <div>
            {selected.map(item => (
              <Chip
                key={item}
                label={`${item[0]} = ${item[1]}`}
                onDelete={() => this.deleteSelected(item)}
                className={classes.chip}
                color="primary"
                style={{ backgroundColor: item[2] }}
              />
            ))
            }
          </div>
        </div>
      );
    }
    return (
      <div>
        No events for selected process
      </div>
    );
  }

  render() {
    const {
      classes,
      component,
      updateComponentsInDrawer,
    } = this.props;
    const {
      expanded,
    } = this.state;

    if (component.inDrawer) {
      return (null);
    }
    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={(
            <Avatar aria-label="Strace" style={{ backgroundColor: component.color }}>
              <img src={component.icon} alt="icon" style={{ height: '20px' }} />
            </Avatar>
          )}
          action={this.headerMenu()}
          title={(
            <div>
              <span>{component.name}</span>
              <IconButton onClick={(e) => { e.preventDefault(); updateComponentsInDrawer(component.id, true); }}>
                <ChevronRightIcon />
              </IconButton>
            </div>
          )}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent className={classes.content}>
            {this.renderGraph()}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}
export default withStyles(styles)(Events);
