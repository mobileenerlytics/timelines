import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import ProcessDropdown from './ProcessDropdown';

const styles = theme => ({
  root: {
    width: '100%',
  },
  nav: {
    backgroundColor: '#88c442',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
});

class Header extends React.PureComponent {
  render() {
    const { classes, processes, value, onChange } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.nav}>
          <Toolbar>
            <Typography className={classes.title} variant="h6" color="inherit" noWrap>
              Eagle Tester
            </Typography>
            <div className={classes.grow} />
            <ProcessDropdown processes={processes} value={value} onChange={onChange} />
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
export default withStyles(styles)(Header);
