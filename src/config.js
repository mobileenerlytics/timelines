import Helpers from './utils/helpers';

import Logcat from './resources/icons/Logcat.png';
import Events from './resources/icons/Events.png';
import Threads from './resources/icons/Threads.png';
import Total from './resources/icons/Total.png';
import CPU from './resources/icons/CPU.png';
import GPU from './resources/icons/GPU.png';
import Screen from './resources/icons/Screen.png';
import GPS from './resources/icons/GPS.png';
import Network from './resources/icons/Network.png';
import CPU0 from './resources/icons/CPU0.png';
import CPU1 from './resources/icons/CPU1.png';
import CPU2 from './resources/icons/CPU2.png';
import CPU3 from './resources/icons/CPU3.png';
import CPU4 from './resources/icons/CPU4.png';
import CPU5 from './resources/icons/CPU5.png';
import CPU6 from './resources/icons/CPU6.png';
import CPU7 from './resources/icons/CPU7.png';
import Decoder from './resources/icons/Decoder.png';
import DRM from './resources/icons/DRM.png';
import SDCard from './resources/icons/SDCard.png';
import Strace from './resources/icons/Strace.png';

export const baseUrl = '';
export const apiUrl = `${baseUrl}/api`;

export const endpoints = {
  processes: {
    url: `${apiUrl}/testrecords/`,
    auth: false,
  },
};

export const phoneIndex = -2;
export const currentSensorIndex = -3;

export const filemapping = {
  logcat: {
    id: 'logcat',
    name: 'Logcat',
    datafor: 'logcat',
    file: 'logcat.json',
    color: Helpers.getColor(0, 400),
    inDrawer: false,
    icon: Logcat,
  },
  events: {
    id: 'events',
    name: 'Events',
    datafor: 'events',
    file: 'events.json',
    color: Helpers.getColor(1, 400),
    inDrawer: false,
    icon: Events,
  },
  threads: {
    id: 'threads',
    name: 'Threads',
    datafor: 'threads',
    file: 'app.json',
    color: Helpers.getColor(2, 400),
    inDrawer: false,
    icon: Threads,
  },
  totalPower: {
    id: 'totalPower',
    name: 'Total',
    datafor: 'total_power',
    file: 'TOTAL.csv',
    color: Helpers.getColor(3, 400),
    inDrawer: false,
    icon: Total,
  },
  cpuPower: {
    id: 'cpuPower',
    name: 'CPU',
    datafor: 'cpu_power',
    file: 'CPU.csv',
    color: Helpers.getColor(4, 400),
    inDrawer: false,
    icon: CPU,
  },
  gpuPower: {
    id: 'gpuPower',
    name: 'GPU',
    datafor: 'gpu_power',
    file: 'GPU.csv',
    color: Helpers.getColor(5, 400),
    inDrawer: false,
    icon: GPU,
  },
  screenPower: {
    id: 'screenPower',
    name: 'Screen',
    datafor: 'screen_power',
    file: 'SCREEN.csv',
    color: Helpers.getColor(6, 400),
    inDrawer: false,
    icon: Screen,
  },
  gpsPower: {
    id: 'gpsPower',
    name: 'GPS',
    datafor: 'gps_power',
    file: 'GPS.csv',
    color: Helpers.getColor(7, 400),
    inDrawer: false,
    icon: GPS,
  },
  network: {
    id: 'network',
    name: 'Network',
    datafor: 'network',
    file: 'network_conversations.json',
    color: Helpers.getColor(8, 400),
    inDrawer: false,
    icon: Network,
  },
  networkPower: {
    id: 'networkPower',
    name: 'Network',
    datafor: 'network_power',
    file: 'network_power.csv',
    color: Helpers.getColor(9, 400),
    inDrawer: false,
    icon: null,
  },
  networkTraffic: {
    id: 'networkTraffic',
    name: 'Network Traffic',
    datafor: 'network_traffic',
    file: 'network_traffic.csv',
    color: Helpers.getColor(10, 400),
    inDrawer: false,
    icon: null,
  },
  cpu0Power: {
    id: 'cpu0Power',
    name: 'CPU 0',
    datafor: 'cpu_0_power',
    file: 'CPU-0.csv',
    color: Helpers.getColor(11, 400),
    inDrawer: true,
    icon: CPU0,
  },
  cpu1Power: {
    id: 'cpu1Power',
    name: 'CPU 1',
    datafor: 'cpu_1_power',
    file: 'CPU-1.csv',
    color: Helpers.getColor(12, 400),
    inDrawer: true,
    icon: CPU1,
  },
  cpu2Power: {
    id: 'cpu2Power',
    name: 'CPU 2',
    datafor: 'cpu_2_power',
    file: 'CPU-2.csv',
    color: Helpers.getColor(13, 400),
    inDrawer: true,
    icon: CPU2,
  },
  cpu3Power: {
    id: 'cpu3Power',
    name: 'CPU 3',
    datafor: 'cpu_3_power',
    file: 'CPU-3.csv',
    color: Helpers.getColor(14, 400),
    inDrawer: true,
    icon: CPU3,
  },
  cpu4Power: {
    id: 'cpu4Power',
    name: 'CPU 4',
    datafor: 'cpu_4_power',
    file: 'CPU-4.csv',
    color: Helpers.getColor(15, 400),
    inDrawer: true,
    icon: CPU4,
  },
  cpu5Power: {
    id: 'cpu5Power',
    name: 'CPU 5',
    datafor: 'cpu_5_power',
    file: 'CPU-5.csv',
    color: Helpers.getColor(16, 400),
    inDrawer: true,
    icon: CPU5,
  },
  cpu6Power: {
    id: 'cpu6Power',
    name: 'CPU 6',
    datafor: 'cpu_6_power',
    file: 'CPU-6.csv',
    color: Helpers.getColor(17, 400),
    inDrawer: true,
    icon: CPU6,
  },
  cpu7Power: {
    id: 'cpu7Power',
    name: 'CPU 7',
    datafor: 'cpu_7_power',
    file: 'CPU-7.csv',
    color: Helpers.getColor(18, 400),
    inDrawer: true,
    icon: CPU7,
  },
  sdcardPower: {
    id: 'sdcardPower',
    name: 'SD Card',
    datafor: 'sdcard',
    file: 'SDCard.csv',
    color: Helpers.getColor(19, 400),
    inDrawer: false,
    icon: SDCard,
  },
  hwDecoderPower: {
    id: 'hwDecoderPower',
    name: 'Hardware Decoder',
    datafor: 'hw_decoder_power',
    file: 'HwDecoder.csv',
    color: Helpers.getColor(20, 400),
    inDrawer: false,
    icon: Decoder,
  },
  drmPower: {
    id: 'drmPower',
    name: 'DRM',
    datafor: 'drm_power',
    file: 'DRM.csv',
    color: Helpers.getColor(21, 400),
    inDrawer: false,
    icon: DRM,
  },
  strace: {
    id: 'strace',
    name: 'Strace',
    datafor: 'strace',
    file: 'strace.json',
    color: Helpers.getColor(22, 400),
    inDrawer: false,
    icon: Strace,
  },
};

export const powerComponents = [
  filemapping.totalPower,
  filemapping.cpuPower,
  filemapping.gpuPower,
  filemapping.screenPower,
  filemapping.gpsPower,
  filemapping.hwDecoderPower,
  filemapping.drmPower,
  filemapping.sdcardPower,
  filemapping.cpu0Power,
  filemapping.cpu1Power,
  filemapping.cpu2Power,
  filemapping.cpu3Power,
  filemapping.cpu4Power,
  filemapping.cpu5Power,
  filemapping.cpu6Power,
  filemapping.cpu7Power,
];

export const drawerComponents = [
  filemapping.logcat,
  filemapping.events,
  filemapping.threads,
  filemapping.totalPower,
  filemapping.cpuPower,
  filemapping.gpuPower,
  filemapping.screenPower,
  filemapping.gpsPower,
  filemapping.network,
  filemapping.networkPower,
  filemapping.networkTraffic,
  filemapping.hwDecoderPower,
  filemapping.drmPower,
  filemapping.sdcardPower,
  filemapping.cpu0Power,
  filemapping.cpu1Power,
  filemapping.cpu2Power,
  filemapping.cpu3Power,
  filemapping.cpu4Power,
  filemapping.cpu5Power,
  filemapping.cpu6Power,
  filemapping.cpu7Power,
  filemapping.strace,
];
