import red from '@material-ui/core/colors/red';
import pink from '@material-ui/core/colors/pink';
import purple from '@material-ui/core/colors/purple';
import deepPurple from '@material-ui/core/colors/deepPurple';
import indigo from '@material-ui/core/colors/indigo';
import blue from '@material-ui/core/colors/blue';
import lightBlue from '@material-ui/core/colors/lightBlue';
import cyan from '@material-ui/core/colors/cyan';
import teal from '@material-ui/core/colors/teal';
import green from '@material-ui/core/colors/green';
import lightGreen from '@material-ui/core/colors/lightGreen';
import lime from '@material-ui/core/colors/lime';
import yellow from '@material-ui/core/colors/yellow';
import amber from '@material-ui/core/colors/amber';
import orange from '@material-ui/core/colors/orange';
import deepOrange from '@material-ui/core/colors/deepOrange';
import brown from '@material-ui/core/colors/brown';
import grey from '@material-ui/core/colors/grey';
import blugrey from '@material-ui/core/colors/blueGrey';

export const mainColors = [
  red,
  pink,
  purple,
  deepPurple,
  indigo,
  blue,
  lightBlue,
  cyan,
  teal,
  green,
  lightGreen,
  lime,
  yellow,
  amber,
  orange,
  deepOrange,
  brown,
  grey,
  blugrey,
];

export default class Helpers {
  static getColor = (number = 0, shade = 500) => {
    let nNumber = number;
    let nShade = shade;
    if (nNumber > 18) {
      while (nNumber >= 19) {
        nNumber -= 19;
      }
      nShade += 100;
    }
    return mainColors[nNumber][nShade];
  };

  static objSearch(object, item, title) {
    if (title) {
      for (let i = 0; i < object.length; i += 1) {
        if (object[i][title] === item) {
          return i;
        }
      }
    } else {
      for (let j = 0; j < object.length; j += 1) {
        if (object[j] === item) {
          return j;
        }
      }
    }
    return -1;
  }

  static toArray = (data, hasLabel) => {
    const lines = data.split('\n');
    const arry = {};
    const first = hasLabel ? 1 : 0; // Process every row except the header
    if (data == null) return [];
    for (let idx = first; idx < lines.length; idx += 1) {
      const line = lines[idx];
      // Oftentimes there's a blank line at the end. Ignore it.
      if (line.length !== 0) {
        const row = line.split(',');
        for (let rowIdx = 0; rowIdx < row.length; rowIdx += 1) {
          // Turn "123" into 123
          row[rowIdx] = parseFloat(row[rowIdx]);
        }
        row[0] = parseInt(row[0], 10);
        const key = row[0];
        arry[key] = row[1];
      }
    }
    return arry;
  }

  static threadsToGraphData = (threads, datafor, duration) => {
    const processId = Object.keys(threads).sort((a, b) => (threads[b].energy - threads[a].energy));
    const processIdSelected = processId.filter(item => threads[item].selected);
    const labels = ['X'].concat(processIdSelected.map(i => threads[i].name));
    const title = (processIdSelected.length === 0) ? 'no data available' : ' ';
    const colors = processIdSelected.map(i => threads[i].color);

    let data = `0\n${duration}`;
    let times = [];
    processIdSelected.filter(id => threads[id][datafor]).forEach((item) => {
      times = [...times, ...Object.keys(threads[item][datafor]).map(i => parseInt(i, 10))];
    });
    times = [...times, duration, 0];
    times = [...new Set(times)].sort((a, b) => a - b);
    times.forEach((time) => {
      let lineData = [];
      lineData = processId.map((item) => {
        if (threads[item][datafor] && Object.keys(threads[item][datafor]) && threads[item][datafor][time]) {
          return threads[item][datafor][time];
        }
        return 0;
      }).filter((item, index) => threads[processId[index]].selected);
      if (lineData.length) {
        if (time === 0) {
          data = `${time},${lineData.join(',')}`;
        } else {
          data = `${data}\n${time},${lineData.join(',')}`;
        }
      }
    });
    return {
      labels,
      title,
      colors,
      data,
    };
  }

  static threadToGraphData = (thread, conversation, datafor, duration) => {
    const dataObject = thread.networks[conversation][datafor] || {};
    const labels = ['X', thread.name];
    const title = ' ';
    const colors = [thread.color];
    let data = `0\n${duration}`;
    let times = [];
    times = [...Object.keys(dataObject).map(i => parseInt(i, 10))];
    times = [...times, duration, 0];
    times = [...new Set(times)].sort((a, b) => a - b);
    times.forEach((time) => {
      const lineData = [dataObject[time]] || [0];
      if (time === 0) {
        data = `${time},${lineData.join(',')}`;
      } else {
        data = `${data}\n${time},${lineData.join(',')}`;
      }
    });
    return {
      labels,
      title,
      colors,
      data,
    };
  }

  static parseCSV = (data) => {
    const regexp = /(-?\d+) : \{(.*?)}/sgm;
    let match = regexp.exec(data);
    const csvData = {};
    while (match != null) {
      const pid = parseInt(match[1], 10);
      const rawCSVData = match[2];
      csvData[pid] = Helpers.toArray(rawCSVData, false);
      match = regexp.exec(data);
    }
    return csvData;
  }

  static myzeropad = (x) => {
    if (x < 10) return `00${x}`;
    if (x < 100) return `0${x}`;
    return x;
  };

  /**
   * @param {Number} date The JavaScript date (ms since epoch)
   * @return {String} A time of the form "HH:MM:SS"
   */
  static valueFormatter = (date) => {
    const d = new Date(date);
    return `${Helpers.myzeropad(d.getMinutes())}:${Helpers.myzeropad(d.getSeconds())}.${Helpers.myzeropad(d.getMilliseconds())}`;
  };

  static valueParser = x => (parseInt(x, 10)) / 1000;
}
