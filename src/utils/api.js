/*
 * API service function
 * responsible for communication to server
 * endpont: object of config
 * array_data : headers
 * array_data : auth
 * array_data : path
 * array_data : params
 * array_data : method
 * array_data : data
 */

// import fetch from 'isomorphic-fetch';
import _Error from './_error';
import Storage from './storage';

class API {
  static request(endpoint, array_data = {}) {
    let headers = {};
    if (array_data.headers) {
      headers = array_data.headers;
    }
    if (!headers['Content-Type']) {
      headers['Content-Type'] = 'application/json';
    }
    if (!headers.Accept) {
      headers.Accept = 'application/json';
    }

    const headerAuth = array_data.auth ? array_data.auth : endpoint.auth;
    if (headerAuth) {
      const token = Storage.getItem('token');
      if (token) {
        headers.Authorization = `Bearer ${token}`;
      }
    }

    let url = (array_data.path) ? endpoint.url + array_data.path.join('/') : endpoint.url;

    if (array_data.params !== null && array_data.params !== {} && array_data.params !== undefined) {
      let params;
      Object.keys(array_data.params).forEach((key) => {
        if (params !== undefined) {
          params = `${params}&${key}=${array_data.params[key]}`;
        } else {
          params = `${key}=${array_data.params[key]}`;
        }
      });
      url = `${url}?${params}`;
    }
    let method = 'get';
    if (array_data.method !== undefined) {
      method = array_data.method;
    } else if (array_data.data !== undefined) {
      method = 'post';
    } else {
      method = 'get';
    }
    const req = {
      method,
      headers: (headers === {}) ? null : headers,
    };
    if (array_data.data) {
      req.body = array_data.data;
    }
    if (req.headers['Content-Type'] === 'application/json') {
      req.body = JSON.stringify(req.body);
    }

    return fetch(url, req)
      .then((response) => {
        const statusCode = response.status;
        let headerContentType = null;
        switch (statusCode) {
          case 200:
          case 201:
          case 204:
            headerContentType = response.headers.get('content-type');
            if (headerContentType !== null && headerContentType.indexOf('application/json') >= 0) {
              return response.json();
            }
            return response.text();
          case 401:
            headerContentType = response.headers.get('content-type');
            if (headerContentType !== null && headerContentType.indexOf('application/json') >= 0) {
              return response.json().then((data) => {
                const returnData = data;
                returnData.status = response.status;
                return returnData;
              }).catch(() => {
                throw new _Error();
              });
            }
            return response.text();
          case 400:
          case 403:
          case 429:
          case 405:
            headerContentType = response.headers.get('content-type');
            if (headerContentType !== null && headerContentType.indexOf('application/json') >= 0) {
              return response.json().then((data) => {
                if (data.error) {
                  throw new _Error(data.error);
                } else if (data.errors) {
                  throw new _Error('', data.errors);
                } else if (data.length > 0) {
                  throw new _Error(data);
                } else {
                  throw new _Error();
                }
              });
            }
            throw new _Error();
          case 404:
            throw new _Error('', { location: '/not-found' });
          default:
            throw new _Error();
        }
      }).catch((response) => {
        if (response instanceof Error) {
          if (response.message === 'Failed to fetch') {
            throw new _Error('Please check your Internet connection');
          }
          throw new _Error(response.message);
        } else {
          throw new _Error();
        }
      });
  }
}

export default API;
