import TimelineService from '../../services/timeline-service';

import { powerComponents, filemapping, phoneIndex } from '../../config';
import Helpers from '../../utils/helpers';

export const LOAD_TIMELINE = 'LOAD_TIMELINE';
export const LOAD_TIMELINE_SUCCESS = 'LOAD_TIMELINE_SUCCESS';
export const LOAD_TIMELINE_FAIL = 'LOAD_TIMELINE_FAIL';

export const CHANGE_CHILD_PROCESS = 'CHANGE_CHILD_PROCESS';

export const LOAD_METADATA = 'LOAD_METADATA';
export const LOAD_METADATA_SUCCESS = 'LOAD_METADATA_SUCCESS';
export const LOAD_METADATA_FAIL = 'LOAD_METADATA_FAIL';

export const LOAD_LOGCAT = 'LOAD_LOGCAT';
export const LOAD_LOGCAT_SUCCESS = 'LOAD_LOGCAT_SUCCESS';
export const LOAD_LOGCAT_FAIL = 'LOAD_LOGCAT_FAIL';

export const LOAD_DATA = 'LOAD_DATA';
export const LOAD_DATA_SUCCESS = 'LOAD_DATA_SUCCESS';
export const LOAD_DATA_FAIL = 'LOAD_DATA_FAIL';

export const LOAD_STRACE = 'LOAD_STRACE';
export const LOAD_STRACE_SUCCESS = 'LOAD_STRACE_SUCCESS';
export const LOAD_STRACE_FAIL = 'LOAD_STRACE_FAIL';

export const LOAD_NETWORK = 'LOAD_NETWORK';
export const LOAD_NETWORK_SUCCESS = 'LOAD_NETWORK_SUCCESS';
export const LOAD_NETWORK_FAIL = 'LOAD_NETWORK_FAIL';

export const LOAD_EVENTS = 'LOAD_EVENTS';
export const LOAD_EVENTS_SUCCESS = 'LOAD_EVENTS_SUCCESS';
export const LOAD_EVENTS_FAIL = 'LOAD_EVENTS_FAIL';

const initialState = {
  loading: false,
  loaded: false,
  id: null,
  processes: {},
  app_loading: {
    meta: false,
    logcat: false,
    // total_power: false, and many others i.e. cpu, gpu, screen etc
  },
  app_failed: {
  },
  meta: {
    duration: 0,
  },
  status: 'pending',
};

export default function timeline(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_TIMELINE:
      return {
        ...state,
        processes: {},
        loading: true,
      };
    case LOAD_TIMELINE_SUCCESS:
      return {
        ...state,
        processes: action.data,
        id: action.id,
        projectId: action.projectId,
        loading: false,
        loaded: true,
        status: 'success',
      };
    case LOAD_TIMELINE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        processes: {},
        status: 'failed',
      };
    case CHANGE_CHILD_PROCESS:
      return {
        ...state,
        processes: action.data,
      };
    case LOAD_METADATA:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          meta: true,
        },
      };
    case LOAD_METADATA_SUCCESS:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          meta: false,
        },
        meta: {
          ...state.meta,
          duration: action.duration,
        },
      };
    case LOAD_METADATA_FAIL:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          meta: false,
        },
        app_failed: {
          ...state.app_failed,
          meta: true,
        },
      };
    case LOAD_LOGCAT:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          logcat: true,
        },
      };
    case LOAD_LOGCAT_SUCCESS:
      return {
        ...state,
        processes: action.data,
        app_loading: {
          ...state.app_loading,
          logcat: false,
        },
      };
    case LOAD_LOGCAT_FAIL:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          logcat: false,
        },
        app_failed: {
          ...state.app_failed,
          logcat: true,
        },
      };
    case LOAD_DATA:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          [action.datafor]: true,
        },
      };
    case LOAD_DATA_SUCCESS:
      return {
        ...state,
        processes: action.data,
        app_loading: {
          ...state.app_loading,
          [action.datafor]: false,
        },
      };
    case LOAD_DATA_FAIL:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          [action.datafor]: false,
        },
        app_failed: {
          ...state.app_failed,
          [action.datafor]: true,
        },
      };
    case LOAD_STRACE:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          strace: true,
        },
      };
    case LOAD_STRACE_SUCCESS:
      return {
        ...state,
        processes: action.data,
        app_loading: {
          ...state.app_loading,
          strace: false,
        },
      };
    case LOAD_STRACE_FAIL:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          strace: false,
        },
        app_failed: {
          ...state.app_failed,
          strace: true,
        },
      };
    case LOAD_NETWORK:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          network: true,
        },
      };
    case LOAD_NETWORK_SUCCESS:
      return {
        ...state,
        processes: action.data,
        app_loading: {
          ...state.app_loading,
          network: false,
        },
      };
    case LOAD_NETWORK_FAIL:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          network: false,
        },
        app_failed: {
          ...state.app_failed,
          network: true,
        },
      };
    case LOAD_EVENTS:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          events: true,
        },
      };
    case LOAD_EVENTS_SUCCESS:
      return {
        ...state,
        processes: action.data,
        app_loading: {
          ...state.app_loading,
          events: false,
        },
      };
    case LOAD_EVENTS_FAIL:
      return {
        ...state,
        app_loading: {
          ...state.app_loading,
          events: false,
        },
        app_failed: {
          ...state.app_failed,
          events: true,
        },
      };
    default:
      return state;
  }
}

export function loadMetaData(id, projectId) {
  return (dispatch) => {
    dispatch({
      type: LOAD_METADATA,
    });
    return TimelineService.getMetaData(id, projectId, 'global.prop')
      .then((data) => {
        const regexpStart = /(startTime)=([0-9]*)/sgm;
        const regexpStop = /(stopTime)=([0-9]*)/sgm;
        const matchStart = regexpStart.exec(data);
        const matchStop = regexpStop.exec(data);
        const duration = parseInt(matchStop[2] / 1000, 10) - parseInt(matchStart[2] / 1000, 10);
        dispatch({
          type: LOAD_METADATA_SUCCESS,
          duration,
        });
      })
      .catch(() => {
        dispatch({
          type: LOAD_METADATA_FAIL,
        });
      });
  };
}

export function loadLogcatData(id, projectId) {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_LOGCAT,
    });
    return TimelineService.getLogcat(id, projectId, filemapping.logcat.file)
      .then((data) => {
        const newList = getState().timeline.processes;
        Object.keys(newList).forEach((key) => {
          if (data[key]) {
            newList[key].threads[key].logcat = data[key];
          }
        });
        dispatch({
          type: LOAD_LOGCAT_SUCCESS,
          data: newList,
        });
      })
      .catch(() => {
        dispatch({
          type: LOAD_LOGCAT_FAIL,
        });
      });
  };
}

export function loadStraceData(id, projectId) {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_STRACE,
    });
    return TimelineService.getStrace(id, projectId, filemapping.strace.file)
      .then((data) => {
        const newList = getState().timeline.processes;
        Object.keys(data).forEach((key) => {
          Object.keys(data[key]).forEach((time) => {
            Object.keys(data[key][time]).forEach((tkey) => {
              if (newList[key] && newList[key].threads[tkey] && data[key][time][tkey].length >= 1) {
                if (newList[key].threads[tkey].strace) {
                  newList[key].threads[tkey].strace[time] = data[key][time][tkey];
                } else {
                  newList[key].threads[tkey].strace = { [time]: data[key][time][tkey] };
                }
              }
            });
          });
        });
        dispatch({
          type: LOAD_STRACE_SUCCESS,
          data: newList,
        });
        return newList;
      })
      .catch(() => {
        dispatch({
          type: LOAD_STRACE_FAIL,
        });
      });
  };
}

export function loadNetworkData(id, projectId) {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_NETWORK,
    });
    return TimelineService.getNetworkConversation(id, projectId, filemapping.network.file)
      .then((data) => {
        const newData = {};
        const newList = getState().timeline.processes;
        if (Object.keys(data).length > 0) {
          return Promise.all([
            TimelineService.getNetworkTraffic(id, projectId, filemapping.networkTraffic.file),
            TimelineService.getNetworkPower(id, projectId, filemapping.networkPower.file),
          ]).then((response) => {
            Object.keys(data).forEach((key) => {
              newData[key] = {};
              Object.keys(data[key]).forEach((nkey) => {
                const { id: nid } = data[key][nkey];
                newData[key][nid] = { ...data[key][nkey] };
                if (response[0][nid]) {
                  newData[key][nid].traffic = response[0][nid];
                }
                if (response[1][nid]) {
                  newData[key][nid].power = response[1][nid];
                }
              });
            });
            Object.keys(newData).forEach((tkey) => {
              if (newList[tkey]) {
                const thread = { ...newList[tkey].threads[tkey] };
                thread.networks = newData[tkey];
                newList[tkey].threads[tkey] = thread;
              }
            });
            dispatch({
              type: LOAD_NETWORK_SUCCESS,
              data: newList,
            });
          });
        }
        return newList;
      })
      .catch(() => {
        dispatch({
          type: LOAD_NETWORK_FAIL,
        });
      });
  };
}

export function loadEventsData(id, projectId) {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_EVENTS,
    });
    return TimelineService.getEvents(id, projectId, filemapping.events.file)
      .then((data) => {
        const newList = getState().timeline.processes;
        let colorIndex = 0;
        Object.keys(data).forEach((key) => {
          const events = [];
          if (newList[key]) {
            Object.keys(data[key]).forEach((ekey) => {
              data[key][ekey].forEach((item) => {
                let color = null;
                events.forEach((event) => {
                  if (event[0] === ekey && event[1] === item.name) {
                    color = event[4];
                  }
                });
                if (color === null) {
                  color = Helpers.getColor(colorIndex);
                  colorIndex += 1;
                }
                events.push([ekey, item.name, item.startTimeMs, item.endTimeMs, color]);
              });
            });
          }
          newList[key].threads[key].events = events;
        });
        dispatch({
          type: LOAD_EVENTS_SUCCESS,
          data: newList,
        });
        return newList;
      })
      .catch(() => {
        dispatch({
          type: LOAD_EVENTS_FAIL,
        });
      });
  };
}

export function loadData(mapping, id, projectId) {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_DATA,
      datafor: mapping.datafor,
    });
    return TimelineService.getData(id, projectId, mapping.file)
      .then((data) => {
        const newList = getState().timeline.processes;
        Object.keys(newList).forEach((key) => {
          Object.keys(newList[key].threads).forEach((tkey) => {
            if (data[tkey]) {
              newList[key].threads[tkey][mapping.datafor] = data[tkey];
            }
          });
        });
        Object.keys(newList).forEach((key) => {
          let allTimeKeys = [];
          Object.keys(newList[key].threads).forEach((tkey) => {
            if (newList[key].threads[tkey][mapping.datafor]) {
              allTimeKeys = [...Object.keys(newList[key].threads[tkey][mapping.datafor]), ...allTimeKeys];
            }
          });
          const dataAddition = {};
          allTimeKeys = [...new Set(allTimeKeys)];
          allTimeKeys.forEach((time) => {
            let maxRangeAtTime = 0;
            Object.keys(newList[key].threads).forEach((tkey) => {
              if (phoneIndex.toString() !== key) {
                if (newList[key].threads[tkey][mapping.datafor] && newList[key].threads[tkey][mapping.datafor][time]) {
                  maxRangeAtTime += newList[key].threads[tkey][mapping.datafor][time];
                }
              } else if (newList[key].threads[tkey][mapping.datafor] && newList[key].threads[tkey][mapping.datafor][time]) {
                if (maxRangeAtTime < newList[key].threads[tkey][mapping.datafor][time]) {
                  maxRangeAtTime = newList[key].threads[tkey][mapping.datafor][time];
                }
              }
            });
            dataAddition[time] = maxRangeAtTime;
          });
          const maxPrev = newList[key].max;
          let maxCur = 0;
          if (Object.keys(dataAddition).length > 0) {
            maxCur = Math.max(...Object.keys(dataAddition).map(item => dataAddition[item]));
          }
          if (maxPrev < maxCur) {
            newList[key].max = maxCur * 1.05;
          }
        });
        dispatch({
          type: LOAD_DATA_SUCCESS,
          datafor: mapping.datafor,
          data: newList,
        });
      })
      .catch(() => {
        dispatch({
          type: LOAD_DATA_FAIL,
          datafor: mapping.datafor,
        });
      });
  };
}

export function loadTimelineData(params) {
  const { id, projectId } = params;
  return (dispatch) => {
    dispatch({
      type: LOAD_TIMELINE,
    });

    return TimelineService.get(id, projectId)
      .then((data) => {
        const newList = data;
        Object.keys(newList).forEach((key) => {
          newList[key].max = 0;
        });
        dispatch({
          type: LOAD_TIMELINE_SUCCESS,
          data: newList,
          id,
          projectId,
        });
        dispatch(loadMetaData(id, projectId));
        dispatch(loadLogcatData(id, projectId));
        powerComponents.forEach(mapping => dispatch(loadData(mapping, id, projectId)));
        dispatch(loadStraceData(id, projectId));
        dispatch(loadNetworkData(id, projectId));
        dispatch(loadEventsData(id, projectId));
      })
      .catch(() => {
        dispatch({
          type: LOAD_TIMELINE_FAIL,
        });
      });
  };
}

export function childProcessSelectChange(parent, child, value) {
  return (dispatch, getState) => {
    const { processes } = getState().timeline;
    const threads = { ...processes[parent].threads };
    const threadChild = { ...threads[child] };
    threadChild.selected = value;
    threads[child] = threadChild;
    processes[parent].threads = threads;
    dispatch({
      type: CHANGE_CHILD_PROCESS,
      data: processes,
    });
  };
}

export function childProcessSelectAll(parent, value) {
  return (dispatch, getState) => {
    const { processes } = getState().timeline;
    const threads = { ...processes[parent].threads };
    Object.keys(threads).forEach((key) => {
      const threadChild = { ...threads[key] };
      threadChild.selected = value;
      threads[key] = threadChild;
    });
    processes[parent].threads = threads;
    dispatch({
      type: CHANGE_CHILD_PROCESS,
      data: processes,
    });
  };
}
