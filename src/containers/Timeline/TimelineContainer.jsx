import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

import { bindActionCreators } from 'redux';
import * as TimelineAction from './timelineReducer';

import { phoneIndex, powerComponents, drawerComponents } from '../../config';

import Loading from '../../components/Loading';
import Header from '../../components/Header';
import LogCat from '../../components/LogCat';
import Strace from '../../components/Strace';
import ScreenVideo from '../../components/ScreenVideo';
import Threads from '../../components/Threads';

import DataPower from '../../components/DataPower';
import Network from '../../components/Network';
import PowerSunBurst from '../../components/PowerSunBurst';
import RightDrawer from '../../components/RightDrawer';
import Helpers from '../../utils/helpers';
import Events from '../../components/Events';
import Failed from '../../components/Failed';

const styles = theme => ({
  root: {
    marginRight: '74px',
  },
  grid: {
    width: '100%',
    margin: `0 ${theme.spacing.unit * 2}px`,
    [theme.breakpoints.down('sm')]: {
      width: 'calc(100% - 20px)',
    },
  },
});

class TimelineContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      processesSelectedValue: null,
      videoCurrentTime: 0,
      xRange: [0, 0],
      yRange: [0, 0],
      components: drawerComponents,
    };
  }

  componentDidMount = () => {
    const { timeline, Timeline } = this.props;
    if ((!timeline.loaded && !timeline.loading) || (timeline.id !== this.getParams().id)) {
      Timeline.loadTimelineData(this.getParams());
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { timeline } = props;
    const { processesSelectedValue } = state;
    const newState = {};
    if (timeline && timeline.processes && (Object.keys(timeline.processes).length > 0) && processesSelectedValue === null) {
      const list = timeline.processes;
      const defaultSelectedValue = parseInt(Object.keys(list).sort((a, b) => (list[b].totalEnergy - list[a].totalEnergy))[0], 10);
      newState.processesSelectedValue = defaultSelectedValue;
    }
    if (timeline.meta.duration > 0 && state.xRange[1] === 0) {
      newState.xRange = [0, timeline.meta.duration];
    }
    let appLoading = false;
    Object.keys(timeline.app_loading).forEach((item) => {
      if (timeline.app_loading[item]) {
        appLoading = true;
      }
    });
    if (processesSelectedValue !== null && timeline.processes[processesSelectedValue].max > 0 && !appLoading && state.yRange[1] === 0) {
      newState.yRange = [0, timeline.processes[processesSelectedValue].max];
    }
    if (processesSelectedValue !== null && !appLoading) {
      const { components } = state;
      const newComponents = [...components];
      Object.keys(timeline.app_failed).forEach((item) => {
        if (timeline.app_failed[item]) {
          const index = Helpers.objSearch(newComponents, item, 'datafor');
          const newComponent = { ...newComponents[index] };
          newComponent.inDrawer = true;
          newComponents[index] = newComponent;
        }
      });
      newState.components = newComponents;
    }
    if (Object.keys(newState).length) {
      return newState;
    }
    return null;
  }

  getParams = (location = window.location) => {
    const searchParams = new URLSearchParams(location.search);
    return {
      id: searchParams.get('id') || '',
      projectId: searchParams.get('projectId') || '',
    };
  }

  processesDropdownChange = (selectedOption) => {
    const { timeline } = this.props;
    if (timeline.processes[selectedOption.value] && timeline.processes[selectedOption.value].max > 0) {
      this.setState({ processesSelectedValue: selectedOption.value, yRange: [0, timeline.processes[selectedOption.value].max] });
    } else {
      this.setState({ processesSelectedValue: selectedOption.value });
    }
  }

  childProcessSelectChange = (child) => {
    const { timeline, Timeline } = this.props;
    const { processesSelectedValue } = this.state;
    Timeline.childProcessSelectChange(processesSelectedValue, child, !timeline.processes[processesSelectedValue].threads[child].selected);
  }

  childProcessSelectAll = (value) => {
    const { Timeline } = this.props;
    const { processesSelectedValue } = this.state;
    if (processesSelectedValue !== null) {
      Timeline.childProcessSelectAll(processesSelectedValue, value);
    }
  }

  videoTimeChange = (time) => {
    this.setState({ videoCurrentTime: time });
  }

  updateRange = (x, y, updateBoth) => {
    const { xRange, yRange } = this.state;
    if (updateBoth) {
      if (x[0] !== xRange[0] || x[1] !== xRange[1] || y[0] !== yRange[0] || y[1] !== yRange[1]) {
        this.setState({ xRange: x, yRange: y });
      }
    } else if (x[0] !== xRange[0] || x[1] !== xRange[1]) {
      this.setState({ xRange: x });
    }
  }

  resetRange = () => {
    const { timeline } = this.props;
    const { processesSelectedValue } = this.state;
    const newState = {};
    if (timeline.meta.duration > 0) {
      newState.xRange = [0, timeline.meta.duration];
    }
    if (processesSelectedValue !== null && timeline.processes[processesSelectedValue].max > 0) {
      newState.yRange = [0, timeline.processes[processesSelectedValue].max];
    }
    this.setState(newState);
  }

  updateComponentsInDrawer = (id, to) => {
    const { components } = this.state;
    const newComponents = [...components];
    const index = Helpers.objSearch(newComponents, id, 'id');
    const newComponent = { ...components[index] };
    newComponent.inDrawer = to;
    newComponents[index] = newComponent;
    this.setState({ components: newComponents });
  }

  getComponent = (id) => {
    const { components } = this.state;
    const index = Helpers.objSearch(components, id, 'id');
    return components[index];
  }

  render() {
    const { timeline, classes } = this.props;
    const {
      processesSelectedValue,
      videoCurrentTime,
      xRange,
      yRange,
      components,
    } = this.state;
    if (timeline.status === 'failed') {
      return (<Failed />);
    }
    if (processesSelectedValue != null) {
      return (
        <Grid container justify="center" className={classes.root}>
          <Header
            processes={timeline.processes}
            value={processesSelectedValue}
            onChange={this.processesDropdownChange}
          />
          <Grid spacing={24} alignItems="stretch" justify="center" container className={classes.grid}>
            <Grid item md={3} xs={12}>
              <ScreenVideo
                id={timeline.id}
                projectId={timeline.projectId}
                videoTimeChange={this.videoTimeChange}
                videoCurrentTime={videoCurrentTime}
              />
              <PowerSunBurst
                threads={timeline.processes[processesSelectedValue].threads || {}}
                processesSelectedValue={processesSelectedValue}
                processes={timeline.processes}
                totalEnergy={timeline.processes[processesSelectedValue].totalEnergy || 0}
              />
            </Grid>
            <Grid item md={9} xs={12}>
              <LogCat
                videoCurrentTime={videoCurrentTime}
                videoTimeChange={this.videoTimeChange}
                logcat={timeline.processes[processesSelectedValue].threads[processesSelectedValue].logcat || {}}
                updateComponentsInDrawer={this.updateComponentsInDrawer}
                component={this.getComponent('logcat')}
              />

              <Events
                videoCurrentTime={videoCurrentTime}
                thread={timeline.processes[processesSelectedValue].threads[processesSelectedValue] || {}}
                processesSelectedValue={processesSelectedValue}
                updateComponentsInDrawer={this.updateComponentsInDrawer}
                component={this.getComponent('events')}
                duration={timeline.meta.duration}
              />
              <Threads
                threads={timeline.processes[processesSelectedValue].threads || {}}
                childProcessSelectChange={this.childProcessSelectChange}
                childProcessSelectAll={this.childProcessSelectAll}
                updateComponentsInDrawer={this.updateComponentsInDrawer}
                component={this.getComponent('threads')}
              />
              {powerComponents.map(mapping => (
                <DataPower
                  stacked={!(phoneIndex === processesSelectedValue)}
                  threads={timeline.processes[processesSelectedValue].threads || {}}
                  duration={timeline.meta.duration}
                  videoCurrentTime={videoCurrentTime}
                  key={mapping.datafor}
                  videoTimeChange={this.videoTimeChange}
                  xRange={xRange}
                  yRange={yRange}
                  updateRange={this.updateRange}
                  resetRange={this.resetRange}
                  updateComponentsInDrawer={this.updateComponentsInDrawer}
                  component={this.getComponent(mapping.id)}
                />
              ))}
              <Network
                networks={timeline.processes[processesSelectedValue].threads[processesSelectedValue].networks || {}}
                thread={timeline.processes[processesSelectedValue].threads[processesSelectedValue] || {}}
                duration={timeline.meta.duration}
                videoCurrentTime={videoCurrentTime}
                videoTimeChange={this.videoTimeChange}
                xRange={xRange}
                yRange={yRange}
                updateRange={this.updateRange}
                resetRange={this.resetRange}
                updateComponentsInDrawer={this.updateComponentsInDrawer}
                component={this.getComponent('network')}
              />
              <Strace
                videoCurrentTime={videoCurrentTime}
                threads={timeline.processes[processesSelectedValue].threads || {}}
                updateComponentsInDrawer={this.updateComponentsInDrawer}
                component={this.getComponent('strace')}
              />
            </Grid>
          </Grid>
          <RightDrawer
            components={components}
            updateComponentsInDrawer={this.updateComponentsInDrawer}
          />
        </Grid>
      );
    }
    return (<Loading />);
  }
}

const mapStateToProps = state => ({
  timeline: state.timeline,
});

const mapActionToProps = dispatch => ({
  Timeline: bindActionCreators(TimelineAction, dispatch),
});

export default withStyles(styles)(connect(
  mapStateToProps,
  mapActionToProps,
)(TimelineContainer));
