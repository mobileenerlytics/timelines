// @flow
/**
 * Service function to make API call
 */
import API from '../utils/api';
import { endpoints, currentSensorIndex, phoneIndex } from '../config';
import Helpers from '../utils/helpers';
// import _Error from 'utils/_error';

export default class TimelineService {
  static requestAPI(endpoint = endpoints.processes, request = {}) {
    return API.request(endpoint, request);
  }

  static get(id: string, projectId: string) {
    return this.requestAPI(undefined, { path: [id, 'app.json'], params: { projectId } }).then((response) => {
      let returnData = {};
      const processesList = {};
      for (let i = 0; i < response.length; i += 1) {
        const process = {};
        process.parentId = parseInt(response[i].parentTaskId, 10);
        process.name = response[i].taskName;
        process.isProc = response[i].mIsProc;
        process.energy = 0;
        process.components = {};
        Object.keys(response[i].perComponentEnergy).forEach((key) => {
          process.energy += parseFloat(response[i].perComponentEnergy[key]);
          process.components[key] = parseFloat(response[i].perComponentEnergy[key]);
        });
        processesList[parseInt(response[i].taskId, 10)] = process;
      }
      const parentProcessesList = {};
      Object.keys(processesList).forEach((pid, ci) => {
        const process = processesList[pid];
        let groupId = 0;
        if (process.isProc) {
          groupId = pid;
        } else {
          let parentProcess = process;
          let parentId = pid;
          while (parentProcess && !parentProcess.isProc) {
            parentId = parentProcess.parentId;
            parentProcess = processesList[parentId];
          }
          groupId = parentId;
        }
        if (!(groupId in parentProcessesList)) {
          parentProcessesList[groupId] = {
            name: processesList[groupId].name,
            totalEnergy: 0,
            threads: {},
          };
        }
        const index = Object.keys(parentProcessesList[groupId].threads).length + ci;
        let color = Helpers.getColor(index, 500);
        if (currentSensorIndex.toString() === pid) {
          color = '#1976d2';
        }
        if (phoneIndex.toString() === pid) {
          color = '#d32f2f';
        }
        parentProcessesList[groupId].threads[pid] = {
          name: process.name,
          energy: process.energy,
          selected: true,
          color,
          components: process.components,
        };
        if (pid !== currentSensorIndex.toString()) {
          parentProcessesList[groupId].totalEnergy += process.energy;
        }
      });
      Object.keys(parentProcessesList).forEach(((key) => {
        if (parentProcessesList[key].totalEnergy > 0) {
          returnData[key] = parentProcessesList[key];
        }
      }));
      return (returnData);
    });
  }

  static getMetaData(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } });
  }

  static getLogcat(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } });
  }

  static getData(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } }).then(data => Helpers.parseCSV(data));
  }

  static getStrace(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } });
  }

  static getNetworkConversation(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } });
  }

  static getNetworkTraffic(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } }).then(data => Helpers.parseCSV(data));
  }

  static getNetworkPower(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } }).then(data => Helpers.parseCSV(data));
  }

  static getEvents(id: string, projectId: string, file: string) {
    return this.requestAPI(undefined, { path: [id, file], params: { projectId } });
  }
}
